#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import os
import sys
try:
    IP_DEST = sys.argv[1]
    PORT = int(sys.argv[2])
    GOJIRA = sys.argv[3]
    SIP_METHODS = ["INVITE", "BYE", "ACK"]
    if len(sys.argv) != 4 or not os.path.exists(GOJIRA):
            print('Usage: python server.py IP port audio_file')
            raise SystemExit
except (IndexError, ValueError):
        sys.exit("Usage: python3 server.py IP_DEST port audio_file")


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""
    def handle(self):
        """Escribe dirección y puerto del cliente (de tupla client_address)."""
        print("La IP y el puerto del cliente es: " + str(self.client_address))
        while 1:
            line = self.rfile.read()
            if not line:
                break
            linea = line.decode('utf-8')
            if linea != ' ':
                print("El cliente nos manda: ", linea)
                (METHOD, address, sip) = linea.split()
                print("Enviando: " + linea)
                if METHOD == 'INVITE':
                    Petition = b"SIP/2.0 100 Trying\r\n\r\n"
                    Petition += b"SIP/2.0 180 Ringing\r\n\r\n"
                    Petition += b"SIP/2.0 200 OK\r\n\r\n"
                    self.wfile.write(Petition)
                if METHOD == 'BYE':
                    Petition = b"SIP/2.0 200 OK\r\n\r\n"
                    self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
                if METHOD == 'ACK':
                    Execute = 'mp32rtp -i 127.0.0.1 -p 23032 < ' + GOJIRA
                    print("Vamos a ejecutar", Execute)
                    os.system(Execute)
                elif METHOD != ("INVITE", "BYE", "ACK"):
                    self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
                else:
                    self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
            else:
                break


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    serv = socketserver.UDPServer((IP_DEST, PORT), EchoHandler)
    print("Listening...")
    serv.serve_forever()
