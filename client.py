#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""
import socket
import sys
# Comprobacion de parametros.
if len(sys.argv) != 3:
    print('Usage: python client.py method receiver@IP:SIPport')
# Comprobar si tiene que salir del programa
if sys.argv[1] != 'INVITE':
    print("\n\n")
    if sys.argv[1] != 'BYE':
        print('Usage: python client.py method receiver@IP:SIPport')
        raise SystemExit

# Cliente UDP simple.
# Dirección IP del servidor.
"""Argumentos."""

METHOD = sys.argv[1]
ADRESS = sys.argv[2].split(':')[0]

# Puerto y Servidor.
try:
    PORT = int(sys.argv[2].split(':')[1])
    SERVER = ADRESS.split('@')[1]
except (IndexError, ValueError):
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto.
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:

    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((SERVER, PORT))

    print("Enviando:", METHOD + ' sip:' + ADRESS + ' SIP/2.0')
    my_socket.send(bytes(METHOD + ' sip:' + ADRESS + ' SIP/2.0', 'utf-8')
                   + b'\r\n')

    try:
        data = my_socket.recv(1024)
        Petition = data.decode('utf-8').split()
    except ConnectionRefusedError:
        print("Error de conexion")
        raise SystemExit
    print('Peticion Recibida -- ', data.decode('utf-8'))
    Rules = [Petition[2] == 'Trying',
             Petition[5] == 'Ringing',
             Petition[8] == 'OK']
    MESSAGE = ('ACK sip:' + ADRESS + ' SIP/2.0')
    if METHOD == 'INVITE' and all(Rules):
        my_socket.send(bytes(MESSAGE, 'utf-8') + b'\r\n\r\n')
    if METHOD == 'BYE'and data.decode('utf-8') == "SIP/2.0 200 OK\r\n\r\n":
        print("Terminando socket...")
